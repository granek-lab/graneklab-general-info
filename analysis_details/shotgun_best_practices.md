# Assembly-Free
- [nf-core/taxprofiler](https://nf-co.re/taxprofiler)
    - [nf-core/taxprofiler manuscript](https://doi.org/10.1101/2023.10.20.563221)
- [Taxonomic classification method for metagenomics based on core protein families with Core-Kaiju](https://pubmed.ncbi.nlm.nih.gov/32633756/)
https://doi.org/10.1101/2023.10.20.563221
## Reviews
- [Evaluation of taxonomic classification and profiling methods for long-read shotgun metagenomic sequencing datasets](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9749362/)
    - Long read data 
        - MEGAN-LR-nuc and MEGAN-LR-prot are recommended 
            - [Pipeline is here](https://github.com/PacificBiosciences/pb-metagenomics-tools)
            - [Reference databases](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-022-05103-0#Sec9) section of paper
    - Short read data
        - mOTUs2 might be better than MetaPhlAn3?
        - also sourmash looks better than Kraken/Bracken/Centrifuge
- [Evaluation of the Microba Community Profiler for Taxonomic Profiling of Metagenomic Datasets From the Human Gut Microbiome](https://pubmed.ncbi.nlm.nih.gov/33959106/)


# Assembly-Based
## MAG Assembly
- NanoPhase [Github](NanoPhase), [publication](https://pubmed.ncbi.nlm.nih.gov/36457010/)

### Reviews
- [Assembly methods for nanopore-based metagenomic sequencing: a comparative study](https://pubmed.ncbi.nlm.nih.gov/32788623/)
- [Critical evaluation of short, long, and hybrid assembly for contextual analysis of antibiotic resistance genes in complex environmental metagenomes](https://pubmed.ncbi.nlm.nih.gov/33580146/)

## Polishing
- [How low can you go? Short-read polishing of Oxford Nanopore bacterial genome assemblies](https://pubmed.ncbi.nlm.nih.gov/38833287/)
## Binning
### Reviews
- [Solving genomic puzzles: computational methods for metagenomic binning](https://pubmed.ncbi.nlm.nih.gov/39082646/)
