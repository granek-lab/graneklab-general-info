# Phyloseq on DCC
 In DCC OnDemand there are a few options for runnning an RStudio image with phyloseq:
 1. In the *MIC Course* App, under *Singularity Image* select *2023 MIC Course*.

 2. Use the *RStudio Apptainer* App and paste this path to the image (this is the same image as above): `/opt/apps/community/od_chsi_rstudio/mic_2023_rstudio.sif`. 
 
 3. Build a custom container image and use the *RStudio Apptainer* App to run it.

# Learning to use Phyloseq

1. Phyloseq vignettes linked from [Bioconductor's Phyloseq page](https://bioconductor.org/packages/release/bioc/html/phyloseq.html)

2. [Official Phyloseq tutorials](https://joey711.github.io/phyloseq/index.html) (pull down menu at top of page)

3. Phyloseq material from the [2023 MIC Course](https://gitlab.oit.duke.edu/mic-course/2023-mic)
	- [Descriptive Statistics session](https://gitlab.oit.duke.edu/mic-course/2023-mic/-/blob/main/content/amplicon/biostats/stat_microbiome_I.Rmd)
	- [Phyloseq object from Kraken output](https://gitlab.oit.duke.edu/mic-course/2023-mic/-/blob/main/content/shotgun/bioinformatics/02_make-phyloseq.Rmd)

4. IBIEM 2020 material
	- [First semester classes](https://github.com/ibiem-2020/ibiem_2020_material/blob/master/content/csp1_schedule.md)
        - [Hands-on: Absolute Abundance](https://github.com/ibiem-2020/ibiem_2020_material/blob/master/content/lessons/absolute_abundance_plots.md)
        - [Hands-on: Alpha Diversity](https://github.com/ibiem-2020/ibiem_2020_material/blob/master/content/lessons/alpha_diversity.md)
        - [Hands-on: Relative Abundance](https://github.com/ibiem-2020/ibiem_2020_material/blob/master/content/lessons/relative_abundance.md)
	- [Second semester classes](https://github.com/ibiem-2020/ibiem_2020_material/blob/master/content/csp2_schedule.md)
		- [Hands-on: Beta Diversity & Ordination](https://github.com/ibiem-2020/ibiem_2020_material/blob/master/content/lessons/ordination.md)
