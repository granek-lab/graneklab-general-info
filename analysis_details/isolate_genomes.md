# Assembly
- [Oxford nanopore long-read sequencing enables the generation of complete bacterial and plasmid genomes without short-read sequencing](https://pubmed.ncbi.nlm.nih.gov/37256057/)
- [HERRO: Nanopore read correction](https://www.biorxiv.org/content/10.1101/2024.05.18.594796v1)