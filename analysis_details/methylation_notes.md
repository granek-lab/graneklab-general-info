# EDA, Statistical, Visualization
## Tutorials and Workflows

- [Epi2ME Modified Base Tutorial](https://labs.epi2me.io/notebooks/Modified_Base_Tutorial.html)
	- [Creating correlation plots from bedMethyl files](https://labs.epi2me.io/how-to-mod/)
- [A cross-package Bioconductor workflow for analysing methylation array data](https://www.bioconductor.org/packages/devel/workflows/vignettes/methylationArrayAnalysis/inst/doc/methylationArrayAnalysis.html)

## Tools and Libraries
- [pycoMeth](https://a-slide.github.io/pycoMeth/)
- [Methplotlib](https://github.com/wdecoster/methplotlib)
	- [Methplotlib for non-CpG data](https://github.com/wdecoster/methplotlib/issues/29)
- [Methylartist](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9154218/)
	- [Methylartist on BioConda](https://bioconda.github.io/recipes/methylartist/README.html)
- [methylSig](https://www.bioconductor.org/packages/devel/bioc/vignettes/methylSig/inst/doc/using-methylSig.html)
- [MethylDackel](https://github.com/dpryan79/MethylDackel#single-cytosine-methylation-metrics-extraction)
- [MethReg](https://bioconductor.org/packages/release/bioc/vignettes/MethReg/inst/doc/MethReg.html)
- [methylPipe](https://bioconductor.org/packages/release/bioc/html/methylPipe.html)
- [methylKit](https://www.bioconductor.org/packages/release/bioc/html/methylKit.html)
- [MethylMix](https://bioconductor.org/packages/release/bioc/html/MethylMix.html)

# Methylation Calling
## Tools and Pipelines
- nanodisco: [Github](https://github.com/fanglab/nanodisco), [Pubmed](https://pubmed.ncbi.nlm.nih.gov/33820988/)

### Older Tools
#### 2019
- [Detection of DNA base modifications by deep recurrent neural network on Oxford Nanopore sequencing data](https://pubmed.ncbi.nlm.nih.gov/31164644/)

## Reviews
- [Latest techniques to study DNA methylation](https://pubmed.ncbi.nlm.nih.gov/31755932/)
- [DNA methylation-calling tools for Oxford Nanopore sequencing: a survey and human epigenome-wide evaluation](https://pubmed.ncbi.nlm.nih.gov/34663425/)

# Biology of DNA Methylation
- [Reassessing 6mA in eukaryotes](https://pubmed.ncbi.nlm.nih.gov/35277704/)
