## Command-line Upload
OOD is just a pretty front end for DCC, so you can use the same tools you would use for DCC (scp, sftp, rsync, Globus). RC has some details here: <https://dcc.duke.edu/dcc/files/#transferring-files>

## GUI Upload
OOD has a GUI-based mechanism for uploading and downloading data. You can get to the OOD file browser by clicking on “Files” in the OOD menu bar. In the OOD file browser you will see *Upload* and *Download* buttons. I do not know how well the GUI handles large amounts of data, so command-line tools may be better for large datasets.

## Globus
- [Data transfers with Globus](https://rc.duke.edu/services/data-transfers-to-and-from-duke-based-storage-resources/)
- [Transferring a file from your local computer to the DCC](https://dcc.duke.edu/globus/dccglobus/)
- [Alternative ID for Globus](https://globus.stanford.edu/accounts/globusid.html)

## Transfer from Cloud Storage
### Using Rclone for Data transfer
#### Running RClone config
Run the following. If a webrowser is needed to generate a token (e.g. OneDrive), this should be run in a terminal from the *DCC Desktop App* in DCC Open OnDemand
```
singularity shell  --bind /work:/work docker://rclone/rclone
```

#### RClone from AWS

Interactive
```
srun -A chsi -p chsi --mem=0 -c 84 singularity pull --force  docker://rclone/rclone
srun --pty -A chsi -p chsi --mem=20G -c 20 singularity shell  --bind /work:/work docker://rclone/rclone

# run the following within the container instantiated above
rclone config
rclone lsf emote:bucket
rclone ls remote:bucket/path
rclone -i copy remote:bucket/path LOCAL_TARGET_PATH
```

Non-interactive
```
srun -A chsi -p chsi --mem=100G -c 30 singularity exec --bind /work:/work docker://rclone/rclone rclone copy --progress --dry-run remote:bucket/path LOCAL_TARGET_PATH
```

#### RClone from Google Drive
```
~/rclone-v1.60.1-linux-amd64/rclone copy --drive-shared-with-me remote:file LOCAL_TARGET_PATH
```

### Alternative Approaches
#### Microsoft OneDrive
Duke's OneDrive seems to block Rclone. The "best" alternative seems to be:
1. Use *DCC Desktop App* in DCC Open OnDemand 
2. Open a webbrowser in the DCC Desktop
3. Open OneDrive in the webbrowser and log in
4. Click download in OneDrive 
    - If downloading a folder, it seems that you need to go into the folder and click download)
    - If download is large, be sure to download somewhere with enough space: /work, a group directory, or datacommons, NOT your home directory
5. If download is a zipfile (e.g. a directory), need to unzip with 7-Zip
```
module load 7-Zip
7zz x OneDrive_FILENAME.zip
```
