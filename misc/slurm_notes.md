# Status
- Show the specs of all nodes in "chsi" partition: `sinfo -o "%17N %10T %13C %10m" -p chsi -N`

- Show the specs of all nodes in "chsi-gpu" partition: `sinfo -o "%17N %10T %13C %10m %10G" -p chsi-gpu -N`

- Show allocated and total memory in "chsi-gpu" partition: `scontrol -o show nodes=dcc-chsi-gpu-0[1-8] | awk '{ print $1, $23, $24}'`

- Show allocated and total memory in "chsi" partition: `scontrol -o show nodes=dcc-chsi-[0-2][0-9] | awk '{ print $1, $23, $24}'`

- What jobs are running on  the "chsi" partition: `squeue  --partition chsi --format="%.18i %.9P %.8j %.8u %.8T %.10M %.9l %.6D %R %C %m"`

# Resource Usage
- Peak memory usage: `sacct  --jobs JOB_ID  --format="MaxRSS,JobID"


# Job submission
- submit a command line via sbatch: `sbatch --wrap "date; hostname; sleep 10; date" --output "/work/$USER/%x-%A-%a.log"`

# Configuration
- Find out what SLURM accounts I belong to: `sacctmgr show user $USER --associations --parsable | cut --delimiter='|' -f 5`