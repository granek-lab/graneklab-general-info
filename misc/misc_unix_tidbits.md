du --apparent-size # can be more accurate on dcc filesystem




# MD5sums
## Parallelize checking MD5 sums
For a directory with many files, this will speed up checking the MD5 sums by spreading it across multiple CPUs

```
NUM_CPUS=80
MD5_CHECKSUM_PATH=XXXXXXXX
FILES_TO_CHECKDIR=XXXXXXXX
MD5_RESULTS_FILE=XXXXXXX
srun -A chsi -p chsi -c $NUM_CPUS --mem 100G bash -c "cat $MD5_CHECKSUM_PATH | parallel -N1 --max-procs $NUM_CPUS --pipe md5sum -c" > $MD5_RESULTS_FILE
```