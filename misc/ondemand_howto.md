
# Starting up RStudio on DCC OnDemand
1. Go to [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
2. Sign in with your NetID and password
3. Click on **Interactive Apps** at the top and select **RStudio Singularity** 
    - If you don't see **Interactive Apps** and only see three lines in the top right, click on the three lines, then **Interactive Apps**, then **RStudio Singularity**
4. A new page should open that says **RStudio Singularity** at the top.
5. For **Account** select **graneklab**
6. For **Partition** select **chsi**
7. Scroll to the bottom and click **Launch**.
8. A new page should open with a box that says **RStudio Singularity**, you may see the following messages:
    - "Please be patient as your job currently sits in queue. The wait time depends on the number of cores as well as time requested."
    - "Your session is currently starting... Please be patient as this process can take a few minutes."
9. You are waiting for a blue button to appear that says **Connect to RStudio Server**. This could take a minute or so. When it appears, click on it.
10. After a minute or so, RStudio should open in your webbrowser. If you get to this point, *you are ready to work!*

> You may need to choose different parameters (CPUs, Memory, Singularity Container), depending on your needs

## Shutting Down
1. In RStudio click on **File** in the top left, then select **Quit Session**
2. A box should pop up that says **R Session Ended**. At this point you can close the tab that contains the RStudio session.
3. In the DCC OnDemand browser window click on the red **Delete** button in the **RStudio Singularity** box, then click **Confirm** in the box that pops up.
4. You can now click on the logout button in the top right, which is an arrow pointing to the right.
5. You are done!

# Starting up Jupyter with GPU on DCC OnDemand
1. Go to [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
2. Sign in with your NetID and password
3. Click on **Interactive Apps** at the top and select **Jupyter Lab Singularity**
    - If you don't see **Interactive Apps** and only see three lines in the top right, click on the three lines, then **Interactive Apps**, then **Jupyter Lab Singularity**
4. A new page should open that says **Jupyter Lab Singularity** at the top.
5. For **Account** select **graneklab** or **chsi**
6. For **Partition** select **chsi-gpu**
7. For **GPU** enter **1**
8. For **Singularity Container File** enter: `/opt/apps/community/od_chsi_jupyter/scanpy_tensorflow_jupyter_latest.sif`
9. Scroll to the bottom and click **Launch**.
10. A new page should open with a box that says **Jupyter Lab Singularity**, you may see the following messages:
    - "Please be patient as your job currently sits in queue. The wait time depends on the number of cores as well as time requested."
    - "Your session is currently starting... Please be patient as this process can take a few minutes."
11. You are waiting for a blue button to appear that says **Connect to Jupyter**. This could take a minute or so. When it appears, click on it.
12. After a minute or so, Jupyter should open in your webbrowser. If you get to this point, *you are ready to work!*
13. To confirm that you have access to a GPU
    1. Click on **Terminal** in the Jupyter Launcher
    2. Run `nvidia-smi` in the terminal. Everything is working if `nvidia-smi` outputs a report like this:

```
Mon Feb 26 16:44:55 2024       
+---------------------------------------------------------------------------------------+
| NVIDIA-SMI 535.104.05             Driver Version: 535.104.05   CUDA Version: 12.2     |
|-----------------------------------------+----------------------+----------------------+
| GPU  Name                 Persistence-M | Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp   Perf          Pwr:Usage/Cap |         Memory-Usage | GPU-Util  Compute M. |
|                                         |                      |               MIG M. |
|=========================================+======================+======================|
|   0  NVIDIA GeForce RTX 2080 Ti     On  | 00000000:13:00.0 Off |                  N/A |
| 29%   22C    P8               6W / 250W |      2MiB / 11264MiB |      0%      Default |
|                                         |                      |                  N/A |
+-----------------------------------------+----------------------+----------------------+
                                                                                         
+---------------------------------------------------------------------------------------+
| Processes:                                                                            |
|  GPU   GI   CI        PID   Type   Process name                            GPU Memory |
|        ID   ID                                                             Usage      |
|=======================================================================================|
|  No running processes found                                                           |
+---------------------------------------------------------------------------------------+
```

> You may need to choose different parameters (CPUs, Memory, Singularity Container), depending on your needs

## Shutting Down
1. In Jupyter click on **File** in the top left, then select **Shut Down**
2. A "Shutdown confirmation" box should pop up, click **Shut Down** 
3. You are done!

