# Important Note
**These notes are specific to a project and should be revised to be more general**

# Setup buckets
- https://portunus-eso.duhs.duke.edu/portunus/
- https://duke.service-now.com/sp?id=kb_article&sys_id=d5afb3a04712b154394bef3c736d43fd

# Prelude to copying
```
srun \
     -A chsi -p chsi \
     --mem=20G -c 10 \
     --pty \
     apptainer shell \
     --bind /datacommons/graneklab \
     docker://rclone/rclone 
```

In the rclone singularity container, can do the following
```
rclone config # this is a one-time setup for your account on DCC (or another computer)
```

```
rclone lsf dhts_aws:dh-dusom-biostat-graneklab-bsf
rclone ls dhts_aws:dh-dusom-biostat-graneklab-bsf
rclone ls dhts_aws:dh-dusom-biostat-graneklab-bsf/ont_dna_data
```

# Copy data to bucket
Use rclone to copy all data from a subdirectory of /datacommons to BSF bucket in dhts aws 
> Remove `--dry-run` to actuall run it

```
NUM_CPUS=10
srun \
     -A chsi -p chsi \
     --mem=20G -c $NUM_CPUS \
     apptainer exec \
     --bind /datacommons/graneklab \
     docker://rclone/rclone \
     rclone copy \
     --dry-run \
     --multi-thread-streams $NUM_CPUS \
     /datacommons/graneklab/projects/bsf_mb_epigenetics \
     dhts_aws:dh-dusom-biostat-graneklab-bsf
```

# Confirming AWS Backups
## Compare Two locations
[rclone check](https://rclone.org/commands/rclone_check/) will confirm that the data is identical in two different location (e.g. the local copy is identical to the copy in the AWS bucket). 

The following code chunk runs `rclone check` to see if the local directory matches the version of the directory that is in the AWS bucket
```
NUM_CPUS=10
srun \
  -A chsi -p chsi \
  --mem=20G -c $NUM_CPUS \
  apptainer exec \
  --bind /work/josh \
  docker://rclone/rclone \
  rclone check \
  --multi-thread-streams $NUM_CPUS \
  /work/josh/for_upload_to_aws/20241209_ont_workshop \
  dhts_aws:dh-dusom-biostat-graneklab-ewd/20241209_ont_workshop
```

## Generate checksum of files in bucket
[rclone md5sum](https://rclone.org/commands/rclone_md5sum/) can be used to enerate md5 checksums from files that are in a bucket
The following code chunk runs `rclone md5sum` to generate MD5 checksums for all the files in the directory on the bucket

```
srun \
  -A chsi -p chsi \
  apptainer exec \
  docker://rclone/rclone \
  rclone md5sum \
  dhts_aws:dh-dusom-biostat-graneklab-ewd/20241209_ont_workshop
```
> Use redirection to save this to a local file, instead of outputing to the terminal

## Confirm that files on AWS match existing checksum
[rclone checksum](https://rclone.org/commands/rclone_checksum/) can compare files to previously generated md5 checksums.
> `rclone md5sum --checkfile` is roughly equivalent to `rclone checksum MD5`, but `rclone checksum MD5` is often preferable because it only outputs a summary and any files that are different, whereas `rclone md5sum --checkfile` also prints, for each file, whether it matches the checksum or not.

The following code chunk runs `rclone checksum MD5` to see if files in the AWS bucket match the checksums in a local md5sum file

```
srun \
  -A chsi -p chsi \
  apptainer exec \
  --bind /work/josh \
  docker://rclone/rclone \
  rclone checksum MD5 \
  /work/josh/for_upload_to_aws/20241209_ont_workshop/commbined_md5sum.txt \
  dhts_aws:dh-dusom-biostat-graneklab-ewd/20241209_ont_workshop
```
> Note that `rclone checksum MD5` and `rclone md5sum --checkfile` do NOT tolerate md5sum checkfiles in non-standard format: they want *two* spaces between the hash and filename and will ignore lines with one one space, reporting "sum not found" for any files on lines with a non-standard separator.

### Remote checksum file
We can also use a checksum file that is *already in the bucket* as input to `rclone md5sum --checkfile` or  `rclone checksum MD5`. Here is an example of that

```
srun \
  -A chsi -p chsi \
  apptainer exec \
  --bind /work/josh \
  docker://rclone/rclone \
  rclone checksum MD5 \
  dhts_aws:dh-dusom-biostat-graneklab-ewd/20241209_ont_workshop/commbined_md5sum.txt \
  dhts_aws:dh-dusom-biostat-graneklab-ewd/20241209_ont_workshop
```
