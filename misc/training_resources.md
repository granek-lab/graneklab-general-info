# Computing
## Duke Resources
- [Roots Workshops](https://colab.duke.edu/roots-workshops/)
	- [Research Computing- Duke Systems Pathway](https://pathways.duke.edu/pathwaypage/31)
	- [High Performance Computing and the Duke Compute Cluster (DCC)](https://pathways.duke.edu/modulepage/144)
- [GCB Genome Academy](https://biostat.duke.edu/gcb-home/education/gcb-academy) sometimes offers Unix workshops

## Bash/UNIX
  - [SWC: The Unix Shell](https://swcarpentry.github.io/shell-novice/)
    - [Download Files for Unix Shell Lesson](misc/unix_shell_setup.Rmd)
  - [SWC: Extra Unix Shell Material](https://carpentries-incubator.github.io/shell-extras/)
- [CEE690: Introduction to Unix](https://gitlab.oit.duke.edu/premierwd/workshops/premier-24-25/-/blob/main/unix_hpc_intro/04_unix_lesson_01.md)
- [Duke Roots: Linux & the Bash Shell](https://pathways.duke.edu/modulepage/142)
- [Coursera: Linux and Bash for Data Engineering](https://www.coursera.org/learn/linux-and-bash-for-data-engineering-duke)
- [BINF 6115 - Introduction to UNIX Computing for Bioinformatics](https://catalog.charlotte.edu/preview_course_nopop.php?catoid=39&coid=134059) at UNC-Charlotte. See [this information about registering](https://ninercentral.charlotte.edu/courses-registration/registration-information/inter-institutional-opportunities/)

## SLURM
- [Introduction to SLURM](https://gitlab.oit.duke.edu/premierwd/workshops/premier-24-25/-/blob/main/unix_hpc_intro/08_slurm_intro.md)
- [Resource Intensive Jobs: srun](misc/srun_slurm.md)
- [SLURM Notes](misc/slurm_notes.md)
- Resource Intensive Jobs: sbatch

# Reproducibility
- [Lecture on Reproducibile Analysis (10/2023)](https://youtu.be/doqwRv7AGMc?si=_PJAUKnHqMNYrMah&t=903)
	- [Lecture Notes](https://gitlab.oit.duke.edu/hiv_r25/2023-2024-hiv-workshop/-/blob/main/data_science/reproducible/reproducible_research_lecture.md)
## Git
- [Hands-on Introduction to Git](https://youtu.be/PJs9cQx4zU4?si=fIMhOPZ_mv1pClVw&t=5521)
	- [Git Overview Notes](https://gitlab.oit.duke.edu/hiv_r25/2023-2024-hiv-workshop/-/blob/main/data_science/reproducible/git_overview.md)
	- [More Git](https://gitlab.oit.duke.edu/hiv_r25/2023-2024-hiv-workshop/-/blob/main/data_science/data_science_TOC.md)

## Containers
- [Introduction to Building Images](https://gitlab.oit.duke.edu/premierwd/workshops/premier-24-25/-/blob/main/unix_hpc_intro/09_image_build_intro.md)
- [Singularity Containers](https://pathways.duke.edu/modulepage/138)
- [Duke Gitlab for Building Singularity Containers](https://gitlab.oit.duke.edu/OIT-DCC/apptainer-example)
- [Example pipeline using multiple microcontainers](scripts/multiple_sifs_example.sh)

## Literate Programming

### Rmarkdown
- Communication between Bash & R Chunks
