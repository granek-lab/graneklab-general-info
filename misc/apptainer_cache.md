If you get an error that includes *"no space left on device"* it probably means that your home directory has reached the quota. A common cause of this is the Apptainer cache. Apptainer (formerly known as Singularity) caches images that you download. By default it caches them in your home directory. This is a problem on DCC where we have a very low home directory quota.

> It is a good idea to do this even if you haven't yet reached your home directory quota

You need to do two things: clean up the cached images and set apptainer to use /work as a cache so it doesn’t happen again. 
> all occurences of "USERNAME" below should be replaced with your actual username (usually your NetID)


Clean up
You need to do this in ssh, not within a container image.
1. Run `du -h --max-depth 1 ~/` to get a baseline for storage usage. I think our quota is 25GB, so that is what I expect you will see. You should probably see most of this in /hpc/home/USERNAME/.apptainer, which is where the image cache defaults

2. Run `apptainer cache list` to see your total cache size.

3. Run `apptainer cache clean` to clean up the cache

4. Now run `du -h --max-depth 1 ~/` and `apptainer cache list` to confirm that the clean up worked.


Change Cache location
To change your cache location you want to add the following to your .bashrc file. RStudio doesn’t seem to want to open the .bashc file, but you can use VS Code (Code Server in DCC OOD) or command line editors like nano, emacs, or vi.

```
export APPTAINER_CACHEDIR="/work/${USER}/.singularity/cache"
export APPTAINER_PULLFOLDER="/work/${USER}/.singularity/pullfolder"
export APPTAINER_TMPDIR="/work/${USER}/tmp”
```
