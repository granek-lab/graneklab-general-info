There are several different storage options on DCC. Most are discussed at https://dcc.duke.edu/dcc/files/ - please read this document!! 

Below are few highlights and group-specific details.

# Storage Locations

## Group Storage
Our group has 1 TB of storage at /hpc/group/graneklab. While 1 TB seems like a lot, it fills up fast, so please be mindful of how you use this space. Raw data should be stored in /hpc/group/graneklab. Please make an appropriate subdirectory for the shared data. You will also want to change permissions after you have placed the data there, so that no one (including yourself) can't accidentally alter or delete it.

Here are some example commands for a project called "my_rnaseq_project"

```
# make new directory
mkdir -p /hpc/group/graneklab/my_rnaseq_project/

# transfer data into the new directory
# [various methods to transfer]

# remove write permissions
chmod -R a-w /hpc/group/graneklab/my_rnaseq_project/
```

## Shared Scratch Space: Working Storage
The `/work` directory is an ideal place to store large output files, particularly intermediate files (e.g. filtered FASTQs, BAMs, etc). It is a good idea to make a subdirectory named based on your Netid in /work, and put output files here. Here is a code snippet that does this.

```
scratch_dir="/work"
username=Sys.info()[["user"]]
out_dir=file.path(scratch_dir,username,"my_rnaseq_project")
dir_create(out_dir)
```
> /work is NOT appropriate for long term storage. It is regularly cleaned (every 60-90 days) by an automatic process. 

## Local Scratch
> This is a power user option, and is not worth the trouble for most situations.

Each of the nodes in the chsi partition has 8TB SSD mounted at /scratch. This is in addition to (and different from) the Shared Scratch Space that is at /work. Because /scratch is local to the node, it is potentially faster than the DCC shared storage (Group Storage, Home Directory, and Shared Scratch). However, because /scratch is local to a node, anything stored there is only available on that node. In other words, if you run a job on dcc-chsi-01 and save output to /scratch, it will not be accessible from dcc-chsi-02. As with Shared Scratch, /scratch is not backed up and files are automatically deleted after 75 days.


# How Much Space Am I Using
The du command tells you how much space is being used by a directory and its sub-directories. The following command will show the usage of jdoe28's sub-directory on the group storage and each of its sub-directories:

du --all --human-readable --max-depth 1 /hpc/group/graneklab/jdoe28

The following will tell you how much space is used and available on the group storage

df -h | egrep 'graneklab|Filesystem'
