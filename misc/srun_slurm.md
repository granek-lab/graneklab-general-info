# Srun
To run commands on compute intensive nodes, including the command line options -A graneklab -p chsi to specify the "graneklab" account (-A graneklab) and the "chsi" partition (-p chsi). For example:

srun -A graneklab -p chsi --pty bash -i

To run commands on GPU nodes include the command line options -A chsi -p chsi-gpu --gres=gpu:1 --mem=14595 -c 2, where

- -A chsi specifies "chsi" account
- -p chsi-gpu specifies "chsi-gpu" partition (the nodes that have GPUs)
- –gres=gpu:1 to actually request a GPU
- –mem=14595 -c 2 to request all available memory and CPUs on the node, since only one jobs can run on a GPU node at a time
For example:

```
srun -A graneklab -p chsi-gpu --gres=gpu:1 --mem=14595 -c 2 --pty bash -i
```
