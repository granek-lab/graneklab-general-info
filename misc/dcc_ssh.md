# Connecting to the DCC by SSH
To connect to the cluster, open a Secure Shell (ssh) session from a terminal using your Duke NetID (replace "NetID" below with your NetID).

```
ssh NetID@dcc-login.oit.duke.edu
```

After running the command you will be prompted for your password. If you are on campus (University or DUHS networks) or connected to the Duke network via the Duke VPN then you will be connected to a login node. If you are off-campus and not connected via the Duke VPN, logging in requires 2-factor authentication and you will also be prompted to enter a 2nd password for your DUO passcode.

# SSH the Easy Way
The following two steps make it a lot quicker and easier to connect to DCC by SSH

## Configure SSH Key-Based Authentication
Using SSH keys for authentication avoids the need for multi-factor authentication (MFA) and entering a password to login to the DCC. These instructions work out of the box if your local computer is MacOS or Linux. For Windows 10 you may need to install SSH.

1. If you don’t already have an SSH key, make one: https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair
2. Add your ssh key to DCC:
   1. Go to https://oit.duke.edu/service/account-self-service/
   2. Click on “Advanced User Options”, then “Update your SSH public keys"
   3. Paste in your SSH key and click “Save Changes"
Once this is set up, you can SSH to DCC without needing to do multi-factor authentication, using the VPN, or even entering a password!

## Set up SSH shortcuts
Add the following to .ssh/config in your home directory (or create the file if it doesn’t exist already) using a text editor. Substitute your netid for “NETID”.

```
Host dcc
    HostName dcc-login.oit.duke.edu
    User NETID
Host dcc1
    HostName dcc-login-01.oit.duke.edu
    User NETID
Host dcc2
    HostName dcc-login-02.oit.duke.edu
    User NETID
Host dcc3
    HostName dcc-login-03.oit.duke.edu
    User NETID
```

Once this is set up you can ssh to the DCC login nodes with the command “ssh dcc” (or to a specific DCC login node with “ssh dcc1”, etc).
