# Notes
The sequencing computer is named *epimetheus*.


# Before Run
1. Prepare a sample manifest describing what is to be sequenced, and how it was prepared including the following. A text-based format (.txt, .csv, .tsv, .md) is preferable
	- sample description
	- DNA extraction kit
	- library prep kit
	- mapping between samples and barcodes [if multiplexing]

2. [Check for Free Space on epimetheus]. The free space necessary will depend on the number and type of flow cells
   - Space Needed:
       - MinION:  **?????? Check with ONT?**
       - P2 Solo, 1 Flow Cell:  **?????? Check with ONT?**
       - P2 Solo, 2 Flow Cells:  **?????? Check with ONT?**

3. If not enough space is available, do one of the following (listed in order from best option to worst option)
       1. Confirm that data has been successfully transferred to DCC, then delete the local copy.
       2. Transfer data to DCC. Confirm that data has been successfully transferred to DCC, then delete the local copy.
       3. Transfer data to external drive. Confirm that data has been successfully transferred to external drive, then delete the local copy.

4. [Check for Free Space on DCC]. If there is not enough space, [Request More Data Commons Space].

# During Run
Run data should be saved to a subdirectory of `/work/ont_data/` on epimetheus


# After Run
## Generate Data Fingerprints
On epimetheus:
1. Open a terminal.
2. `cd` to the top level directory for the sequencing run. The top level directory could have a variety of names, depending on how the run was set up. It will generally contains the HTML report and either the `no_sample` subdirectory, or subdirectories for each barcode.
3. Add the sample manifest file to this directory.
4. Run the following command to generate a file with MD5 checksums for all sequencing data

```
find -type f \( -not -name "md5sum.txt" \) -exec md5sum '{}' \; > md5sum.txt
```
	
## Transfer data to DCC
The Data parent directory (DPD) referred to below is on DCC, the path is `/hpc/group/graneklab/projects/premier`.
Note that the DPD is likely to change in the near future, when we exceed the quota on our group directory

On DCC, create a subdirectory of DPD named YYYY_MM_DD, where YYYY MM, and DD are the year, month, and day that sequencing run was *started*. This subdirectory will be the Raw Data Directory (RDD)

There are two options for transferring data to DCC: Globus and rsync

### Transfer Data with Globus
1. Go to https://www.globus.org/
2. Click on **LOG IN** in the upper right corner
3. Click **FILE MANAGER** on the left side
4. In **Collection** Search, type "Duke Compute Cluster"
5. **Duke Compute Cluster (DCC) Data Transfer Node** should appear in the search results, click on it.
6. In the **File Manager** view, change to RDD. This can be done by typing/pasting the RDD path into the **Path** bar, or by navigating using the directory view below it.
7. Click on **Upload** (Click **Continue** If you get the message: "Authentication/Consent is required for the Globus web app to perform HTTPS uploads on your behalf.")
8. Click **Select Folder to Upload**. In the file dialog select the top level directory for the sequencing run. Globus should start uploading.

### Transfer Data with rsync
**Fix Me**
*Set up SSH*
`rsync -a --progress --stats /work/ont_data/2023_11_29_jg_demo dcc:/hpc/group/graneklab/projects/premier`



# Follow-up

## Confirm that transfer was successful
Data fingerprints need to be validated in order to confirm that transfer of data to DCC was successful.

1. `ssh` to dcc [It is recommended to start a tmux session], but not required.
2. `cd` to the RDD
3. Run md5sum with the following command:
```srun -A chsi -p chsi md5sum -c md5sum.txt```

This will take a while to run. The length of time depends on the amount of data, but it could be an hour or more. As individual files are checked, the file name will be output followed by *OK* or *FAILED*. 

If there was a problem with the data transfer, you will get an error message like the following (where "NNN" is the number of files that had a problem)
```
/usr/bin/md5sum: WARNING: NNN computed checksum did NOT match
srun: error: dcc-chsi-23: task 0: Exited with exit code 1
```

If all data was transferred successfully, after the last file name there will *not be a message*, there will just be a new shell prompt.

## Set permissions to 


## Clean up
Once you have confirmed that the transfer to DCC was successful, delete the data from epimetheus to ensure that there is space for future sequencing runs.

## Upload Sequence Data Files to SRA (recommended)
It is strongly recommended to upload data to SRA as soon as possible. Uploading data to SRA provides an additional backup!!

If this is done immediately after sequencing, data should be embargoed for as long as possible (5 years).

Note: SRA does NOT accept POD5 files, so POD5 files will need to be converted to FAST5 files before upload.

# Details
## Check for Free Space on DCC
Run the following command on DCC to check for free space `df -h /hpc/group/graneklab/`. Look under the **Avail** column to see if the amount of space listed is at least as much as you expect to need.

## Check for Free Space on epimetheus
Run the following command on DCC to check for free space `df -h /work/`. Look under the **Avail** column to see if the amount of space listed is at least as much as you expect to need.

## Check for Free Space on external drive
1. Plug the external drive into the electrical outlet and turn on switch on the back of the drive
2. Plug USB cord from external drive into USB port on epimetheus
3. Run `df -h /backup` in a terminal. Look under the **Avail** column to see if the amount of space listed is at least as much as you expect to need. *Note:* the results from the `df` command should say `/backup` under *Mounted on* (like below), if it instead says `/`, it means that the external drive did not correctly attach to epimetheus.

```
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda1        13T   32K   12T   1% /backup
```


## Disconnecting external drive
Before disconnecting the external drive from epimetheus you must either Power Off epimetheus, or "unmount" the drive. To unmount the drive:
1. Look for the drive's icon in the dock on the desktop - it will be a grey rectangle with the USB icon in it (if you put the mouse pointer over it it should say "p2_solo_backup_drive").
2. Right-click on the icon and select "Unmount"
3. At the top of the screen there a message should appear that the drive has been unmounted (it will only be there briefly then disappear)
4. It is now safe to disconnect the external drive and turn off its switch


## Transfer Data to external drive
**FIX ME**

`rsync -a --progress --stats /work/ont_data/2023_11_29_jg_demo /backup/ont_data`




## Request More Data Commons Space
Contact Dr. Granek if the amount of free space on DCC is less than you expect to need. 

# TODO
- Look into globus CLI?
  - Globus sync https://github.com/chrisdjscott/globus_sync_directory/blob/main/README.md
  - https://docs.globus.org/cli/
- SRA uploads
  - add details for upload to SRA
  - figure out if FASTQs need to be uploaded along with POD5/FAST5 
