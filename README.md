# Computing
- [Training Resources](misc/training_resources.md)

## DCC Computing Environment
- [DCC OnDemand URL](https://dcc-ondemand-01.oit.duke.edu/)
- [Getting started with DCC OnDemand](misc/ondemand_howto.md)
- [Using DCC by SSH](misc/dcc_ssh.md)

### Data Management
- [Data Storage](misc/data_storage.md)
- [Data Transfer (Upload/Download)](misc/data_up_down.md)
- [Data Backup on DHTS AWS](misc/data_backup.md)



# Microbiome Analysis
  - Big Picture Overviews of Microbiome Sequencing and Analysis Methods
    - [Sequencing-based analysis of microbiomes](https://pubmed.ncbi.nlm.nih.gov/38918544/)
    - []
  - [Phyloseq](analysis_details/using_phyloseq.md)
  - [Shotgun Analysis](analysis_details/shotgun_best_practices.md) 

# Microbial Genomics
  - [DNA Methylation](analysis_details/methylation_notes.md)
  - [Isolate Genome Assembly and Annotation](analysis_details/isolate_genomes.md)

# Troubleshooting
- ["no space left on device"](misc/apptainer_cache.md)
